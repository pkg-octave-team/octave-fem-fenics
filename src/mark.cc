/*
 Copyright (C) 2014 Eugenio Gianniti <eugenio.gianniti@mail.polimi.it>

 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 3 of the License, or (at your option) any later
 version.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, see <http://www.gnu.org/licenses/>.
*/

#include <dolfin.h>
#include "meshfunction.h"
#include "subdomain.h"
#include "dolfin_compat.h"

DEFUN_DLD (mark, args, nargout,
"-*- texinfo -*-\n\
@deftypefn {Function File} {[@var{markers}]} = \
mark (@var{subdomain}, @var{meshfunction}, @var{number})\n\
Mark @var{meshfunction} with @var{number} on the entities contained in \
@var{subdomain}. The output, @var{markers}, is a copy of @var{meshfunction} \
where entities inside @var{subdomain} are marked.\n\
@seealso{MeshFunction, SubDomain}\n\
@end deftypefn")
{
  int nargin = args.length ();
  octave_value retval;

  if (nargin < 3 || nargin > 3 || nargout > 1)
    { print_usage (); }
  else
    {
      if (! meshfunction_type_loaded)
        {
          meshfunction::register_type ();
          meshfunction_type_loaded = true;
          mlock ();
        }
      if (! subdomain_type_loaded)
        {
          subdomain::register_type ();
          subdomain_type_loaded = true;
          mlock ();
        }

      if (args(0).type_id () == subdomain::static_type_id () &&
          args(1).type_id () == meshfunction::static_type_id () &&
          args(2).is_real_scalar ())
        {
          std::size_t number = args(2).ulong_value ();
          subdomain const & sd = static_cast <subdomain const &>
            (args(0).get_rep ());
          meshfunction const & mf = static_cast <meshfunction const &>
            (args(1).get_rep ());

          if (! error_state)
            {
              dolfin::MeshFunction <std::size_t> out = mf.get_mf ();
              SHARED_PTR <subdomain_rep const> const & sdrep = sd.get_psd ();

              sdrep->mark (out, number);
              retval = new meshfunction (mf.get_str (), out);
            }
        }
      else
        { error ("invalid input arguments"); }
    }

  return retval;
}

/*
 Copyright (C) 2014 Eugenio Gianniti <eugenio.gianniti@mail.polimi.it>

 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 3 of the License, or (at your option) any later
 version.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __FEMFENICS_BASE_FACTORY__
#define __FEMFENICS_BASE_FACTORY__

#include <dolfin.h>
#include <octave/oct.h>

class femfenics_base_factory
{
public:

  femfenics_base_factory () {}
  virtual ~femfenics_base_factory () {}

  virtual octave_value matrix (dolfin::Matrix const &) const = 0;
  virtual octave_value vector (dolfin::Vector const &) const = 0;
};

#endif
/*
 Copyright (C) 2013 Marco Vassallo <gedeone-octave@users.sourceforge.net>

 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 3 of the License, or (at your option) any later
 version.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, see <http://www.gnu.org/licenses/>.
*/

#include "function.h"
#include "mesh.h"
#include "Plot_2d.h"
#include "Plot_3d.h"
#include "dolfin_compat.h"

DEFUN_DLD (plot, args, , "-*- texinfo -*-\n\
@deftypefn {Function File} \
plot (@var{Mesh}, @var{Nodal_Values}(OPTIONAL))\n\
Plot a Mesh. \n\
The input parameter is the Mesh and optionally also a vector representing \
the values of a function at each node.\n\
@seealso{Mesh, save}\n\
@end deftypefn")
{

  int nargin = args.length ();
  octave_value retval;
  
  if (nargin < 1 || nargin > 2)
    print_usage ();
  else
    {
      if (! mesh_type_loaded)
        {
          mesh::register_type ();
          mesh_type_loaded = true;
          mlock ();
        }

      if (args(0).type_id () == mesh::static_type_id ())
        {
          const mesh & msho = static_cast<const mesh&> (args(0).get_rep ());

          if (nargin == 1)
            {
              if (!error_state)
                {
                  const SHARED_PTR <const dolfin::Mesh>
                    & mshd = msho.get_pmsh ();
                  dolfin::plot (*mshd);
                  dolfin::interactive ();
                  retval = 0;
                }
             }

          else if (nargin == 2)
            {
              Array <double> myu = args(1).array_value ();

              if (!error_state)
                {
                  const SHARED_PTR <const dolfin::Mesh> & mshd = msho.get_pmsh ();
                  SHARED_PTR <const dolfin::FunctionSpace> V;
                  uint D = mshd->topology ().dim ();
                  if (D == 2)
                    {
                      SHARED_PTR <const dolfin::FunctionSpace>
                        Vt (new Plot_2d::FunctionSpace (mshd));
                      V = Vt;
                    }

                  else if(D == 3)
                    {
                      SHARED_PTR <const dolfin::FunctionSpace>
                        Vt (new Plot_3d::FunctionSpace (mshd));
                      V = Vt;
                    }

                  else
                    error ("Plot: The size of the mesh must be 2 or 3");


                  if (V->dim () != myu.numel ())
                    error ("Plot: The size of the functional space\
                            and the size of the vector must agree");
                  else
                    {
#ifdef LATEST_DOLFIN
                      dolfin::Vector du (MPI_COMM_WORLD, myu.numel ());
#else
                      dolfin::Vector du(myu.numel ());
#endif
                      for (std::size_t i = 0; i < myu.numel (); ++i)
                        du.setitem (i, myu(i));

                      SHARED_PTR <dolfin::Vector>
                        uu (new dolfin::Vector(du));

                      SHARED_PTR <const dolfin::Function>
                        u (new dolfin::Function(V, uu));

                      dolfin::plot (*u);
                      dolfin::interactive ();

                      retval = 0;
                    }
                }
            }
        }

    }
  return retval;
}

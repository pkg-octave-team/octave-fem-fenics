/*
 Copyright (C) 2014 Eugenio Gianniti <eugenio.gianniti@mail.polimi.it>

 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 3 of the License, or (at your option) any later
 version.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, see <http://www.gnu.org/licenses/>.
*/

#include <dolfin.h>
#include <octave/oct.h>
#include "dolfin_compat.h"

DEFUN_DLD (barrier, args, nargout,
"-*- texinfo -*-\n\
@deftypefn {Function File} {} \
barrier ()\n\
Set an MPI barrier.\n\
@end deftypefn")
{
  octave_value retval;
  int nargin = args.length ();

  if (nargin > 0 || nargout > 0)
    { print_usage (); }
  else
    {
      dolfin::MPI::barrier (
#ifdef LATEST_DOLFIN
                            MPI_COMM_WORLD
#endif
                           );
    }

  return retval;
}

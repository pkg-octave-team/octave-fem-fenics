/*
 Copyright (C) 2013 Marco Vassallo <gedeone-octave@users.sourceforge.net>

 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 3 of the License, or (at your option) any later
 version.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, see <http://www.gnu.org/licenses/>.
*/

#include "coefficient.h"

DEFUN_DLD (Expression, args, ,
"-*- texinfo -*-\n\
@deftypefn {Function File} {[@var{f}]} = \
Expression (@var{name}, @var{Function_handle})\n\
Creates an object with the value specified as a function handle.\n\
The input parameters are\n\
@itemize @bullet \n\
@item @var{name} is the name of the coefficient as it is declared in the ufl \
file\n\
@item @var{Function_handle} is a function handle which specify the expression \
to apply for our coefficient\n\
@end itemize\n\
The output @var{f} is an object which contains a representation \
of the function\n\
@seealso{Constant, Function}\n\
@end deftypefn")
{
  int nargin = args.length ();
  octave_value retval;

  if (nargin < 2 || nargin > 2)
    print_usage ();
  else
    {
      std::string str = args(0).string_value ();
      octave_fcn_handle * fh = args(1).fcn_handle_value ();

      if (! coefficient_type_loaded)
        {
          coefficient::register_type ();
          coefficient_type_loaded = true;
          mlock ();
        }

      if (!error_state)
        {
          octave_value_list b (3, 1);
          octave_value_list tmp = feval (fh->function_value (), b);
          Array<double> res = tmp(0).array_value ();
          std::size_t l = res.numel ();

          /*
          Look at the discussion 
          http://fenicsproject.org/qa/781/scalar-and-vector-valued-expression
          */
          if (l > 1)
            retval = new coefficient (str, *fh, l);
          else
            retval = new coefficient (str, *fh);
        }

   }

  return retval;
}

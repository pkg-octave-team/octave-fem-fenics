/*
 Copyright (C) 2013 Marco Vassallo <gedeone-octave@users.sourceforge.net>

 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 3 of the License, or (at your option) any later
 version.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _BOUNDARYCONDITION_OCTAVE_
#define _BOUNDARYCONDITION_OCTAVE_

#include <memory>
#include <vector>
#include <dolfin.h>
#include <octave/oct.h>
#include "dolfin_compat.h"

class boundarycondition : public octave_base_value
{

public:

  boundarycondition ()
    : octave_base_value () {}

  void
  print (std::ostream & os, bool pr_as_read_syntax = false) const
  {
    for (std::size_t i = 0; i < bcu.size (); ++i)
      { os << "Boundary condition : " << bcu[i]->str (true) << std::endl; }
  }

  ~boundarycondition (void) {}

  bool
  is_defined (void) const
  { return true; }

  const std::vector< SHARED_PTR
    <const dolfin::DirichletBC> > &
  get_bc (void) const
  { return bcu; }

  void
  add_bc (const SHARED_PTR <const dolfin::FunctionSpace> & V,
          SHARED_PTR <const dolfin::GenericFunction> f,
          std::size_t n)
  {
    SHARED_PTR <const dolfin::DirichletBC>
      p (new dolfin::DirichletBC (V, f, n));
    bcu.push_back (p);
  }

  void
  add_bc (SHARED_PTR <dolfin::FunctionSpace const> const & V,
          SHARED_PTR <dolfin::GenericFunction const> f,
          SHARED_PTR <dolfin::MeshFunction <std::size_t> const> const & sd,
          std::size_t n)
  {
    SHARED_PTR <dolfin::DirichletBC const>
      p (new dolfin::DirichletBC (V, f, sd, n));
    bcu.push_back (p);
  }

private:

  std::vector<SHARED_PTR
    <const dolfin::DirichletBC> > bcu;

#ifdef DECLARE_OCTAVE_ALLOCATOR
  DECLARE_OCTAVE_ALLOCATOR;
#endif
  DECLARE_OV_TYPEID_FUNCTIONS_AND_DATA;

};

static bool boundarycondition_type_loaded = false;

#ifdef DEFINE_OCTAVE_ALLOCATOR
DEFINE_OCTAVE_ALLOCATOR (boundarycondition);
#endif
DEFINE_OV_TYPEID_FUNCTIONS_AND_DATA (boundarycondition,
                                     "boundarycondition",
                                     "boundarycondition");
#endif

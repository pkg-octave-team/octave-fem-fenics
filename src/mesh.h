/*
 Copyright (C) 2013 Marco Vassallo <gedeone-octave@users.sourceforge.net>

 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 3 of the License, or (at your option) any later
 version.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _MESH_OCTAVE_
#define _MESH_OCTAVE_

#include <dolfin.h>
#include <octave/oct.h>
#include <octave/oct-map.h>
#include "dolfin_compat.h"

class mesh : public octave_base_value
{

 public:

  mesh ()
    : octave_base_value () {}

  mesh (const dolfin::Mesh& _msh)
    : octave_base_value (), pmsh (new dolfin::Mesh(_msh)) {}

  mesh (Array<double>& p,
        Array<octave_idx_type>& e,
        Array<octave_idx_type>& t);

  mesh (std::string _filename)
    : octave_base_value (), pmsh (new dolfin::Mesh(_filename)) {}

  void
  print (std::ostream& os, bool pr_as_read_syntax = false) const
    { 
      os << "msh : " << pmsh ->label ()
      << " with " << pmsh -> num_vertices ()
      << " vertices " << std::endl;
    }

  ~mesh(void) {}

  bool
  is_defined (void) const 
    { return true; }

  const dolfin::Mesh & get_msh (void) const
  { return *pmsh; }

  const SHARED_PTR <const dolfin::Mesh> & 
  get_pmsh (void) const
    { return pmsh; }

  octave_scalar_map 
  get_pet (void) const;

 private:

  SHARED_PTR <const dolfin::Mesh> pmsh;

#ifdef DECLARE_OCTAVE_ALLOCATOR
  DECLARE_OCTAVE_ALLOCATOR;
#endif
  DECLARE_OV_TYPEID_FUNCTIONS_AND_DATA;
};

static bool mesh_type_loaded = false;

#ifdef DEFINE_OCTAVE_ALLOCATOR
DEFINE_OCTAVE_ALLOCATOR (mesh);
#endif
DEFINE_OV_TYPEID_FUNCTIONS_AND_DATA (mesh, "mesh", "mesh");
#endif

/*
 Copyright (C) 2014 Eugenio Gianniti <eugenio.gianniti@mail.polimi.it>

 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 3 of the License, or (at your option) any later
 version.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, see <http://www.gnu.org/licenses/>.
*/

#include "femfenics_factory.h"
#include "PETSc_factory.h"
#include <octave/parse.h>

femfenics_base_factory const &
femfenics_factory::factory (void) const
{
  std::string backend = linear_algebra_backend ();

  // Default back-end
  return PETSc_factory::instance ();
}

std::string
femfenics_factory::linear_algebra_backend (void) const
{
  octave_value_list ovl = feval ("linear_algebra_backend");
  std::string retval = ovl (0).string_value ();
  return retval;
}

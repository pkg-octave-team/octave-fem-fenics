/*
 Copyright (C) 2014 Eugenio Gianniti <eugenio.gianniti@mail.polimi.it>

 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 3 of the License, or (at your option) any later
 version.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __FEMFENICS_PETSC_FACTORY__
#define __FEMFENICS_PETSC_FACTORY__

#include "femfenics_base_factory.h"

class PETSc_factory : public femfenics_base_factory
{
public:

  virtual ~PETSc_factory () {}

  octave_value matrix (dolfin::Matrix const &) const;
  octave_value vector (dolfin::Vector const &) const;

  static femfenics_base_factory const & instance (void);

private:

  PETSc_factory () {}
  PETSc_factory (PETSc_factory const &);
  PETSc_factory & operator = (PETSc_factory const &);

  static void add_to_arrays (Array <octave_idx_type> &,
                             Array <octave_idx_type> &,
                             Array <double> &,
                             std::vector <std::size_t> const &,
                             std::vector <std::size_t> const &,
                             std::vector <double> const &);

  octave_value do_matrix_serial (dolfin::Matrix const &) const;
  octave_value do_matrix_parallel (dolfin::Matrix const &) const;
  octave_value do_vector_serial (dolfin::Vector const &) const;
  octave_value do_vector_parallel (dolfin::Vector const &) const;
};

#endif

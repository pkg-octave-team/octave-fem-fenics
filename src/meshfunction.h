/*
 Copyright (C) 2014 Eugenio Gianniti <eugenio.gianniti@mail.polimi.it>

 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 3 of the License, or (at your option) any later
 version.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _MESHFUNCTION_OCTAVE_
#define _MESHFUNCTION_OCTAVE_

#include <dolfin.h>
#include <octave/oct.h>
#include "dolfin_compat.h"

class meshfunction : public octave_base_value
{
public:

  meshfunction (void)
    : octave_base_value () {}

  meshfunction (std::string const & label,
                dolfin::MeshFunction <std::size_t> const & _mf)
    : octave_base_value (), pmf (new dolfin::MeshFunction <std::size_t> (_mf)),
      str (label) {}

  meshfunction (std::string const & label,
                SHARED_PTR <dolfin::Mesh const> mesh,
                std::string const & filename)
    : octave_base_value (),
      pmf (new dolfin::MeshFunction <std::size_t> (mesh, filename)),
      str (label) {}

  meshfunction (std::string const & label,
                SHARED_PTR <dolfin::Mesh const> mesh,
                std::size_t dim, std::size_t value)
    : octave_base_value (),
      pmf (new dolfin::MeshFunction <std::size_t> (mesh, dim, value)),
      str (label) {}

  bool
  is_defined (void) const
  { return true; }

  void
  print (std::ostream & os, bool pr_as_read_syntax = false) const
  { os << "MeshFunction <std::size_t>: " << get_pmf ()->str (true) << std::endl; }

  dolfin::MeshFunction <std::size_t> const &
  get_mf (void) const
  { return *pmf; }

  SHARED_PTR <dolfin::MeshFunction <std::size_t> const> const &
  get_pmf (void) const
  { return pmf; }

  std::string const &
  get_str (void) const
  { return str; }

private:

  SHARED_PTR <dolfin::MeshFunction <std::size_t> const> pmf;
  std::string str;

#ifdef DECLARE_OCTAVE_ALLOCATOR
  DECLARE_OCTAVE_ALLOCATOR;
#endif
  DECLARE_OV_TYPEID_FUNCTIONS_AND_DATA;
};

static bool meshfunction_type_loaded = false;

#ifdef DEFINE_OCTAVE_ALLOCATOR
DEFINE_OCTAVE_ALLOCATOR (meshfunction);
#endif
DEFINE_OV_TYPEID_FUNCTIONS_AND_DATA (meshfunction, "meshfunction", "meshfunction");

#endif

/*
 Copyright (C) 2014 Eugenio Gianniti <eugenio.gianniti@mail.polimi.it>

 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 3 of the License, or (at your option) any later
 version.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _SUBDOMAIN_REP_OCTAVE_
#define _SUBDOMAIN_REP_OCTAVE_

#include <dolfin.h>
#include <octave/oct.h>
#include <octave/oct-map.h>
#include <octave/ov-fcn-handle.h>
#include <octave/ov-fcn.h>
#include <octave/parse.h>
#include <octave/octave.h>

class subdomain_rep : public dolfin::SubDomain
{
public:

  subdomain_rep (void)
    : dolfin::SubDomain (), pfh (NULL) {}

  subdomain_rep (octave_fcn_handle const & fh, bool on_boundary)
    : dolfin::SubDomain (), pfh (new octave_fcn_handle (fh)),
      check_boundary (on_boundary) {}

  ~subdomain_rep (void) { delete pfh; }

  bool
  inside (dolfin::Array<double> const & x, bool on_boundary) const
  {
    octave_value_list b;
    b.resize (x.size ());
    for (std::size_t i = 0; i < x.size (); ++i)
      { b(i) = x[i]; }
    octave_value_list tmp = feval (pfh->function_value (), b);
    bool retval = tmp(0).bool_value ();
    if (check_boundary)
      { retval = retval && on_boundary; }
    return retval;
  }

private:

  octave_fcn_handle * pfh;
  bool check_boundary;
};
#endif

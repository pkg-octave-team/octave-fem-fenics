/*
 Copyright (C) 2013 Marco Vassallo <gedeone-octave@users.sourceforge.net>

 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 3 of the License, or (at your option) any later
 version.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _FORM_OCTAVE_
#define _FORM_OCTAVE_

#include <memory>
#include <vector>
#include <dolfin.h>
#include <octave/oct.h>
#include "dolfin_compat.h"

class form : public octave_base_value
{

 public:

  form ()
    : octave_base_value () {}

  form (const dolfin::Form _frm)
    : octave_base_value (), frm (new dolfin::Form (_frm)) {}

  form (SHARED_PTR <const dolfin::Form> _frm)
    : octave_base_value (), frm (_frm) {}

  void
  print (std::ostream& os, bool pr_as_read_syntax = false) const
    {  
       os << "Form " << ": is a form of rank " << frm->rank ()
       << " with " << frm->num_coefficients () 
       << " coefficients" << std::endl; 
    }

  ~form(void) {}

  bool
  is_defined (void) const 
    { return true; }

  const dolfin::Form & 
  get_form (void) const
    { return (*frm); }

  const SHARED_PTR <const dolfin::Form> & 
  get_pform (void) const
    { return frm; }

 private:

  SHARED_PTR <const dolfin::Form> frm;

#ifdef DECLARE_OCTAVE_ALLOCATOR
  DECLARE_OCTAVE_ALLOCATOR;
#endif
  DECLARE_OV_TYPEID_FUNCTIONS_AND_DATA;

};
static bool form_type_loaded = false;

#ifdef DEFINE_OCTAVE_ALLOCATOR
DEFINE_OCTAVE_ALLOCATOR (form);
#endif
DEFINE_OV_TYPEID_FUNCTIONS_AND_DATA (form, "form", "form");
#endif

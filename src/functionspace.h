/*
 Copyright (C) 2013 Marco Vassallo <gedeone-octave@users.sourceforge.net>

 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 3 of the License, or (at your option) any later
 version.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _FUNCTIONSPACE_OCTAVE_
#define _FUNCTIONSPACE_OCTAVE_

#include <memory>
#include <vector>
#include <dolfin.h>
#include <octave/oct.h>
#include "dolfin_compat.h"

class functionspace : public octave_base_value
{

 public:

  functionspace () 
    : octave_base_value (), fsp () {}

  functionspace (SHARED_PTR <const dolfin::FunctionSpace> _fsp)
    : octave_base_value (), fsp (_fsp) {}

  void
  print (std::ostream& os, bool pr_as_read_syntax = false) const
    { os << "Functional Space : " << (*fsp).str(true) << std::endl; }

  ~functionspace(void) {}

  bool
  is_defined (void) const 
    { return true; }

  const dolfin::FunctionSpace &
  get_fsp (void) const
    { return (*fsp); }

  const SHARED_PTR <const dolfin::FunctionSpace> &
  get_pfsp (void) const
    { return fsp; }

  void
  set_fsp (dolfin::FunctionSpace & _fsp)
    {
      dolfin::FunctionSpace * p = new dolfin::FunctionSpace (_fsp);
      fsp = SHARED_PTR <const dolfin::FunctionSpace> (p);
    }

 private:

  SHARED_PTR <const dolfin::FunctionSpace> fsp;

#ifdef DECLARE_OCTAVE_ALLOCATOR
  DECLARE_OCTAVE_ALLOCATOR;
#endif
  DECLARE_OV_TYPEID_FUNCTIONS_AND_DATA;

};

static bool functionspace_type_loaded = false;

#ifdef DEFINE_OCTAVE_ALLOCATOR
DEFINE_OCTAVE_ALLOCATOR (functionspace);
#endif
DEFINE_OV_TYPEID_FUNCTIONS_AND_DATA (functionspace,
                                     "functionspace",
                                     "functionspace");

#endif

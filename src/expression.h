/*
 Copyright (C) 2013 Marco Vassallo <gedeone-octave@users.sourceforge.net>

 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 3 of the License, or (at your option) any later
 version.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _EXPRESSION_OCTAVE_
#define _EXPRESSION_OCTAVE_

#include <dolfin.h>
#include <octave/oct.h>
#include <octave/oct-map.h>
#include <octave/ov-fcn-handle.h>
#include <octave/ov-fcn.h>
#include <octave/parse.h>
#include <octave/octave.h>

class expression : public dolfin::Expression
{

 public:
  expression()
    : dolfin::Expression() {}

  expression (octave_fcn_handle & _f)
    : dolfin::Expression (), f (new octave_fcn_handle (_f)) {}

  expression (octave_fcn_handle & _f,
              std::size_t _n)
    : dolfin::Expression (_n), f (new octave_fcn_handle (_f)) {}

  ~expression (void) { delete f; }

  void 
  eval (dolfin::Array<double>& values,
        const dolfin::Array<double>& x) const
    {
      octave_value_list b;
      b.resize (x.size ());
      for (std::size_t i = 0; i < x.size (); ++i)
        b(i) = x[i];
      octave_value_list tmp = feval (f->function_value (), b);
      Array<double> res = tmp(0).array_value ();

      for (std::size_t i = 0; i < values.size (); ++i)
        values[i] = res(i);
    }

 private:
  octave_fcn_handle * f;
};
#endif

/*
 Copyright (C) 2013 Marco Vassallo <gedeone-octave@users.sourceforge.net>

 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 3 of the License, or (at your option) any later
 version.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, see <http://www.gnu.org/licenses/>.
*/

#include "function.h"
#include "dolfin_compat.h"

DEFUN_DLD (plot, args, , "-*- texinfo -*-\n\
@deftypefn {Function File} \
plot (@var{Function})\n\
Plot a Function. \n\
@seealso{Function, Save}\n\
@end deftypefn")
{

  int nargin = args.length ();
  octave_value retval;
  
  if (nargin < 1 || nargin > 1)
    print_usage ();
  else
    {
      if (! function_type_loaded)
        {
          function::register_type ();
          function_type_loaded = true;
          mlock ();
        }

      if (args(0).type_id () == function::static_type_id ())
        {
          const function & uo =
            static_cast<const function&> (args(0).get_rep ());

          if (!error_state)
            {
              const SHARED_PTR <const dolfin::Function>
                & u = uo.get_pfun ();

              dolfin::plot (*u);
              dolfin::interactive ();

              retval = 0;
            }
        }
    }
  return retval;
}

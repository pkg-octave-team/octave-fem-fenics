/*
 Copyright (C) 2013 Marco Vassallo <gedeone-octave@users.sourceforge.net>

 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 3 of the License, or (at your option) any later
 version.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _FUNCTION_OCTAVE_
#define _FUNCTION_OCTAVE_

#include <memory>
#include <vector>
#include <dolfin.h>
#include <octave/oct.h>
#include <octave/parse.h>
#include "dolfin_compat.h"

class function : public octave_base_value
{

public:

  function ()
    : octave_base_value (), fun () {}

  function (std::string & _str,
            SHARED_PTR <const dolfin::Function> _fun)
    : octave_base_value (), str (_str), fun (_fun) {}

  function (function const & _func)
    : octave_base_value (), str (_func.get_str ()), fun (_func.get_pfun ()) {}

  void
  print (std::ostream & os,
         bool pr_as_read_syntax = false) const
  { os << "Function " << str << ": " << fun->str (true) << std::endl; }

  ~function (void) {}

  bool
  is_defined (void) const
  { return true; }

  const dolfin::Function &
  get_fun (void) const
  { return (*fun); }

  const SHARED_PTR <const dolfin::Function> &
  get_pfun (void) const
  { return fun; }

  void
  set_fun (dolfin::Function & _fun)
  {
    dolfin::Function * p = new dolfin::Function (_fun);
    fun = SHARED_PTR <const dolfin::Function> (p);
  }

  const std::string &
  get_str (void) const
  { return str; }

  octave_value
  subsref (const std::string & type,
           const std::list<octave_value_list> & idx)
  {
    octave_value retval;
    retval = subsref (type, idx, 1);
    return retval;
  }

  octave_value_list
  subsref (const std::string & type,
           const std::list<octave_value_list> & idx,
           int nargout)
  {
    octave_value_list retval;

    switch (type[0])
      {
      case '(':
        {
          retval = do_multi_index_op (nargout, idx.front ());
        }
        break;

      case '{':
      case '.':
        {
          std::string nm = type_name ();
          error ("%s cannot be indexed with %c", nm.c_str (), type[0]);
        }
        break;

      default:
        { panic_impossible (); }
      }

    return retval;
  }

  octave_value_list
  do_multi_index_op (int nargout, const octave_value_list & idx)
  {
    octave_value_list retval;
    std::list<octave_value_list> args (1, idx);
    args.push_front (octave_value (new function (*this)));
    retval = feval ("feval", args, nargout);
    return retval;
  }

private:

  std::string str;
  SHARED_PTR <const dolfin::Function> fun;

#ifdef DECLARE_OCTAVE_ALLOCATOR
  DECLARE_OCTAVE_ALLOCATOR;
#endif
  DECLARE_OV_TYPEID_FUNCTIONS_AND_DATA;

};
static bool function_type_loaded = false;

#ifdef DEFINE_OCTAVE_ALLOCATOR
DEFINE_OCTAVE_ALLOCATOR (function);
#endif
DEFINE_OV_TYPEID_FUNCTIONS_AND_DATA (function, "function", "function");
#endif

/*
 Copyright (C) 2013 Marco Vassallo <gedeone-octave@users.sourceforge.net>

 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 3 of the License, or (at your option) any later
 version.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _COEFFICIENT_OCTAVE_
#define _COEFFICIENT_OCTAVE_

#include "expression.h"
#include "dolfin_compat.h"

class coefficient : public octave_base_value
{

 public:
  coefficient ()
    : octave_base_value () {}

  coefficient (std::string & _str,
               octave_fcn_handle & _f)
    : str (_str), expr (new expression (_f)) {}

  coefficient (std::string & _str,
               octave_fcn_handle & _f,
               std::size_t _n)
    : str (_str), expr (new expression (_f, _n)) {}

  void
  print (std::ostream& os, bool pr_as_read_syntax = false) const
    {
      os << "Coefficient " << str << " : " <<  expr->str (true) << std::endl;
    }

  ~coefficient (void) {}

  bool
  is_defined (void) const 
    { return true; }

  const SHARED_PTR <const expression> & 
  get_expr (void) const
    { return expr; }

  const std::string &
  get_str (void) const
    { return str; }

 private:

  std::string str;
  SHARED_PTR <const expression> expr;

#ifdef DECLARE_OCTAVE_ALLOCATOR
  DECLARE_OCTAVE_ALLOCATOR;
#endif
  DECLARE_OV_TYPEID_FUNCTIONS_AND_DATA;

};
static bool coefficient_type_loaded = false;

#ifdef DEFINE_OCTAVE_ALLOCATOR
DEFINE_OCTAVE_ALLOCATOR (coefficient);
#endif
DEFINE_OV_TYPEID_FUNCTIONS_AND_DATA (coefficient,
                                     "coefficient",
                                     "coefficient");

#endif

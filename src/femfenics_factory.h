/*
 Copyright (C) 2014 Eugenio Gianniti <eugenio.gianniti@mail.polimi.it>

 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 3 of the License, or (at your option) any later
 version.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program; if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __FEMFENICS_FACTORY__
#define __FEMFENICS_FACTORY__

#include "femfenics_base_factory.h"
#include <string>

class femfenics_factory : public femfenics_base_factory
{
public:

  femfenics_factory ()
  { dolfin::parameters["linear_algebra_backend"] = linear_algebra_backend (); }

  virtual ~femfenics_factory () {}

  virtual inline octave_value matrix (dolfin::Matrix const & A) const
  { return factory ().matrix (A); }

  virtual inline octave_value vector (dolfin::Vector const & b) const
  { return factory ().vector (b); }

private:

  femfenics_factory (femfenics_factory const &);
  femfenics_factory operator = (femfenics_factory const &);

  std::string linear_algebra_backend (void) const;
  femfenics_base_factory const & factory (void) const;
};

#endif
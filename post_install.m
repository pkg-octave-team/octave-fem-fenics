## Copyright (C) 2013 Marco Vassallo <gedeone-octave@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

function post_install (desc)

  include = fullfile (desc.dir, "include");
  [status, msg] = mkdir (include);
  if (status != 1)
    error ("couldn't create include/ directory: %s", msg);
  endif

  private = fullfile (include, "fem-fenics");
  [status, msg] = mkdir (private);
  if (status != 1)
    error ("couldn't create include/fem-fenics/ directory: %s", msg);
  endif

  [status, msg, msgid] = movefile ('./src/*.h', private, 'f');
  if (status != 1)
    error ("couldn't copy file: %s", msg);
  endif
endfunction

## Copyright (C) 2013 Marco Vassallo <gedeone-octave@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {Function File} {[@var{c}]} = Constant (@var{name}, @var{value})
##
## Creates a constant object over all the mesh elements with the value 
## specified.
##
## This function take as input the @var{name} of the Constant that has
## to be created and its @var{value}, which can be either a scalar or a vector.
##
## @seealso{Expression, Function} 
## @end deftypefn

function c = Constant (name, xx)

  if nargin != 2
    error ("Constant: wrong number of input parameters.");
  elseif ! ischar (name)
    error ("Constant: second argument is not a valid string");
  elseif ! isvector (xx)
    error ("Constant: second argument is not a valid string");
  endif

  c = Expression (name, @(x, y, z) xx);

endfunction

## Copyright (C) 2014 Eugenio Gianniti <eugenio.gianniti@mail.polimi.it>
##
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.


## -*- texinfo -*-
## @deftypefn {Function File} {} import_meshfunction_type (@var{typename})
## Compile a wrapper for dolfin::MeshFunction <@var{typename}>
## @seealso {MeshFunction}
## @end deftypefn

function import_meshfunction_type (typename)

  if (is_master_node ())
    if (nargin != 1)
      error ("import_meshfunction_type: wrong number of input parameters.");
    elseif (! ischar (typename))
      error ("import_meshfunction_type: first argument is not a valid string");
    endif

    typename = strtrim (typename);

    # Ensure that the label does not contain whitespace or symbols
    label = genvarname (strjoin (strsplit (typename), ""));

    if (! check_mf_files (typename))
      n = length (mfilename ("fullpath")) - length (mfilename ());
      path = strtrunc (mfilename ("fullpath"), n);

      private = fullfile (path, "include/");
      output = generate_mf_header (typename, label);
      output += generate_mf_constructor (label);
      output += generate_mf_save (typename, label);
      output += generate_mf_makefile (label, private);

      if (output != 0)
        error ("compilation failed");
      else
        [output, textfile] = ...
          system (["make -f Makefile_", label, " all"]);
        if (output != 0)
          display (text);
          error ("compilation failed");
        endif
        [output, textfile] = system (["make -f Makefile_", label, " clean"]);
        if (output != 0)
          display (text);
          error ("compilation failed");
        endif
      endif
    endif
  endif

  barrier ();

endfunction

## Copyright (C) 2013 Marco Vassallo <gedeone-octave@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

pkg load fem-fenics msh
problem = 'HyperElasticity';
import_ufl_Problem (problem);

Rotation = @(x,y,z) ...
   [0; ...
   0.5*(0.5 + (y - 0.5)*cos(pi/3) - (z-0.5)*sin(pi/3) - y);...
   0.5*(0.5 + (y - 0.5)*sin(pi/3) + (z-0.5)*cos(pi/3) - z)];

#Create mesh and define function space
x = y = z = linspace (0, 1, 17);
mshd = Mesh (msh3m_structured_mesh (x, y, z, 1, 1:6));
V  = FunctionSpace (problem, mshd);

# Create Dirichlet boundary conditions
bcl = DirichletBC (V, @(x,y,z) [0; 0; 0], 1);
bcr = DirichletBC (V, Rotation, 2);
bcs = {bcl, bcr};
 
# Define source and boundary traction functions
B = Constant ('B', [0.0; -0.5; 0.0]);
T = Constant ('T', [0.1; 0.0; 0.0]);

# Set material parameters
E = 10.0;
nu = 0.3;
mu = Constant ('mu', E./(2*(1+nu)));
lmbda = Constant ('lmbda', E*nu./((1+nu)*(1-2*nu)));
u = Expression ('u', @(x,y,z) [0; 0; 0]);

# Create (linear) form defining (nonlinear) variational problem
L = ResidualForm (problem, V, mu, lmbda, B, T, u);

# Solve nonlinear variational problem F(u; v) = 0

# Create function for the resolution of the NL problem
  function [y, jac] = f (problem, xx, V, bc1, bc2, B, T, mu, lmbda)
    u = Function ('u', V, xx);
    a = JacobianForm (problem, V, V, mu, lmbda, u);
    L = ResidualForm (problem, V, mu, lmbda, B, T, u);
    if (nargout == 1)
      [y, xx] = assemble (L, xx, bc1, bc2);
    elseif (nargout == 2)
      [jac, y, xx] = assemble_system (a, L, xx, bc1, bc2);
    endif
  endfunction

u0 = assemble (L, bcs{:});
fs = @(xx) f (problem, xx, V, bcl, bcr, B, T, mu, lmbda);
[x, fval, info] = fsolve (fs, u0, optimset ("jacobian", "on"));
func = Function ('u', V, x);
 
# Save solution in VTK format
save (func, 'displacement');

# Plot solution
plot (func);

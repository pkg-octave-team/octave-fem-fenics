## Copyright (C) 2013 Marco Vassallo <gedeone-octave@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.


pkg load fem-fenics msh

problem = 'Biharmonic';
import_ufl_Problem (problem);

# Create mesh and define function space
x = y = linspace (0, 1, 33);
mesh = Mesh(msh2m_structured_mesh (x, y, 1, 1:4));

V = FunctionSpace(problem, mesh);

bc = DirichletBC (V, @(x,y) 0, 1:4);

f = Expression ('f', @(x,y) 4.0*pi^4.*sin(pi.*x).*sin(pi.*y));
g = Expression ('alpha', @(x,y) 8);

a = BilinearForm (problem, V, V, g);
L = LinearForm (problem, V, f);

[A, b] = assemble_system (a, L, bc);
u = A \ b;

func = Function ('u', V, u);
plot (func);

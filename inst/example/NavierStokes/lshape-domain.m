## Copyright (C) 2013 Marco Vassallo <gedeone-octave@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.


name = [tmpnam ".geo"];
fid = fopen (name, "w");
fputs (fid,"Point (1)  = {0, 0, 0, 0.1};\n");
fputs (fid,"Point (2)  = {1, 0, 0, 0.1};\n");
fputs (fid,"Point (3)  = {1, 0.5, 0, 0.1};\n");
fputs (fid,"Point (4)  = {0.5, 0.5, 0, 0.1};\n");
fputs (fid,"Point (5) = {0.5, 1, 0, 0.1};\n");
fputs (fid,"Point (6) = {0, 1, 0,0.1};\n");

fputs (fid,"Line (1)  = {5, 6};\n");
fputs (fid,"Line (2) = {2, 3};\n");

fputs (fid,"Line(3) = {6,1,2};\n");
fputs (fid,"Line(4) = {5,4,3};\n");
fputs (fid,"Line Loop(7) = {3,2,-4,1};\n");
fputs (fid,"Plane Surface(8) = {7};\n");
 fclose (fid);
 msho = msh2m_gmsh (canonicalize_file_name (name)(1:end-4),"scale", 1,"clscale", .2);
 trimesh (msho.t(1:3,:)', msho.p(1,:)', msho.p(2,:)');
 unlink (canonicalize_file_name (name));

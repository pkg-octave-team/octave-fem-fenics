## Copyright (C) 2013 Marco Vassallo <gedeone-octave@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.


## -*- texinfo -*-
## @deftypefn {Function File} {@var{V}} = @
## FunctionSpace (@var{myproblem}, @var{mesh})
##
## Generate a FunctionSpace on a specific mesh.
##
## This function takes as input the name @var{myproblem} of the ufl file where 
## the FunctionSpace is defined and the @var{mesh} where it has to be 
## created.
##
## @seealso{FunctionSpace, SubSpace, import_ufl_FunctionSpace} 
## @end deftypefn

function V = FunctionSpace (name, mesh)

  if nargin != 2
    error ("FunctionSpace: wrong number of input parameters.");
  elseif ! ischar (name)
    error ("FunctionSpace: second argument is not a valid string");
  endif
  program = sprintf ("%s_FunctionSpace(mesh)", name);

  V = eval (program);

endfunction

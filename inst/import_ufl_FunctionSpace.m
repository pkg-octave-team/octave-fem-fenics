## Copyright (C) 2013 Marco Vassallo <gedeone-octave@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.


## -*- texinfo -*-
## @deftypefn {Function File} {} = import_ufl_FunctionSpace (@var{myproblem})
## Import a FunctionSpace from a ufl file.
##
## @var{myproblem} is the name of the ufl file where 
## the FunctionSpace is defined. This function creates in the working directory
## a file called @var{myproblem_FunctionSpace.oct}.
##
## @seealso{import_ufl_Problem, FunctionSpace, BilinearForm, LinearForm, 
## Functional} 
## @end deftypefn

function import_ufl_FunctionSpace (var_prob)

  if (is_master_node ())
    if (nargin != 1)
      error ("import_ufl_FunctionSpace: wrong number of input parameters.");
    elseif (! ischar (var_prob))
      error ("import_ufl_FunctionSpace: first argument is not a valid string");
    endif

    if (check_hash (var_prob) || ! check_oct_files (var_prob, "FunctionSpace"))
      n = length (mfilename ("fullpath")) - length (mfilename ());
      path = strtrunc (mfilename ("fullpath"), n);

      private = fullfile (path, "include/");
      output = generate_fs (var_prob);
      output += generate_makefile (var_prob, private);

      if (output != 0)
        error ("Compilation failed");
      else
        [output, textfile] = system ...
          (sprintf ("make -f Makefile_%s fs", var_prob));
        if (output != 0)
          display (text);
          error ("Compilation failed");
        endif
        [output, textfile] = system (sprintf ("make -f Makefile_%s clean", var_prob));
        if (output != 0)
          display (text);
          error ("Compilation failed");
        endif
        save_hash (var_prob);
      endif
    endif
  endif

  barrier ();

endfunction

## Copyright (C) 2013 Marco Vassallo <gedeone-octave@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {Function File} {[@var{J}]} = @
## JacobianForm (@var{my_problem}, @var{U}, @var{V}, @var{coefficient_1}, @
## @var{coefficient_2},...)
##
## Construct a JacobianForm previously imported from a ufl file with the 
## function import_ufl_BilinearForm.
##
## The compulsory arguments are:
## @itemize @bullet
## @item @var{my_problem}, the name of the problem to solve.
## @item the FunctionSpace @var{U} and @var{V} where the problem is defined.
## @end itemize
##
## The optional arguments are the @var{coefficient_1}, @var{coefficient_2} 
## which specify the parameters for the JacobianForm with the same name which
## was used in the ufl file.
## They can be either a Constant, a Function or an Expression.
##
## @seealso{import_ufl_BilinearForm, LinearForm, ResidualForm, BilinearForm}
## @end deftypefn

function a = JacobianForm (name, U, V, varargin)

  if nargin < 2
    error ("JacobianForm: wrong number of input parameters.");
  elseif ! ischar (name)
    error ("JacobianForm: first argument is not a valid string");
  endif

  program = sprintf ("%s_BilinearForm(U, V", name);
   for k = 1:length (varargin)
      eval(['f_' num2str(k) '=varargin{k};']);
      program = strjoin ({ program, strcat('f_',num2str(k))}, ',');
   end

  program = strjoin ({program, ');'});
  a = eval (program);

endfunction

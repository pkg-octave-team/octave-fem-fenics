## Copyright (C) 2014 Eugenio Gianniti <eugenio.gianniti@mail.polimi.it>
##
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.


## -*- texinfo -*-
## @deftypefn {Function File} @var{val} = linear_algebra_backend ()
## @deftypefnx {Function File} @var{old_val} = linear_algebra_backend (@var{new_val})
##
## Query or set the internal variable that specifies the linear algebra back-end
## to use when assembling systems, matrices or vectors. It defaults to "PETSc".
## Currently, only "PETSc" is implemented in fem-fenics.
##
## @end deftypefn

function output = linear_algebra_backend (varargin)

  persistent backend = "PETSc";
  persistent opts = {"PETSc"};

  if (nargin > 1)
    print_usage ();
  else
    output = backend;
    if (nargin == 1)
      input = validatestring (varargin{1}, opts,
                              "linear_algebra_backend",
                              "new_val");
      backend = input;
    endif
  endif

endfunction

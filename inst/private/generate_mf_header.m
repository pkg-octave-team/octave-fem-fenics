## Copyright (C) 2014-2016 Eugenio Gianniti <eugenio.gianniti@polimi.it>
##
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.


## -*- texinfo -*-
## function for internal usage only
## @end deftypefn

function output = generate_mf_header (typename, label)

STRING ="\n\
#ifndef _MESHFUNCTION_@@LABEL@@_OCTAVE_\n\
#define _MESHFUNCTION_@@LABEL@@_OCTAVE_\n\
\n\
#include <dolfin.h>\n\
#include <octave/oct.h>\n\
#include <fem-fenics/dolfin_compat.h>\n\
\n\
class meshfunction_@@LABEL@@ : public octave_base_value\n\
{\n\
public:\n\
\n\
  meshfunction_@@LABEL@@ (void)\n\
    : octave_base_value () {}\n\
\n\
  meshfunction_@@LABEL@@ (dolfin::MeshFunction <@@TYPENAME@@ > const & _mf)\n\
    : octave_base_value (), pmf (new dolfin::MeshFunction <@@TYPENAME@@ > (_mf)) {}\n\
\n\
  meshfunction_@@LABEL@@ (SHARED_PTR <dolfin::Mesh const> mesh,\n\
                          std::string const & filename)\n\
    : octave_base_value (),\n\
      pmf (new dolfin::MeshFunction <@@TYPENAME@@ > (mesh, filename)) {}\n\
\n\
  bool\n\
  is_defined (void) const\n\
  { return true; }\n\
\n\
  void\n\
  print (std::ostream& os, bool pr_as_read_syntax = false) const\n\
  { os << ""MeshFunction <@@TYPENAME@@ >: ""\n\
       << get_pmf ()->str (@@VERBOSE@@) << std::endl; }\n\
\n\
  dolfin::MeshFunction <@@TYPENAME@@ > const &\n\
  get_mf (void) const\n\
  { return *pmf; }\n\
\n\
  SHARED_PTR <dolfin::MeshFunction <@@TYPENAME@@ > const> const &\n\
  get_pmf (void) const\n\
  { return pmf; }\n\
\n\
private:\n\
\n\
  SHARED_PTR <dolfin::MeshFunction <@@TYPENAME@@ > const> pmf;\n\
\n\
#ifdef DECLARE_OCTAVE_ALLOCATOR\n\
  DECLARE_OCTAVE_ALLOCATOR;\n\
#endif\n\
  DECLARE_OV_TYPEID_FUNCTIONS_AND_DATA;\n\
};\n\
\n\
static bool meshfunction_@@LABEL@@_type_loaded = false;\n\
\n\
#ifdef DEFINE_OCTAVE_ALLOCATOR\n\
DEFINE_OCTAVE_ALLOCATOR (meshfunction_@@LABEL@@);\n\
#endif\n\
DEFINE_OV_TYPEID_FUNCTIONS_AND_DATA (meshfunction_@@LABEL@@,\n\
""meshfunction_@@LABEL@@"", ""meshfunction_@@LABEL@@"");\n\
\n\
#endif";

STRING = strrep (STRING, "@@TYPENAME@@", typename);
STRING = strrep (STRING, "@@LABEL@@", label);
verbose = "false";
if (strcmp (typename, "double"))
  verbose = "true";
endif
STRING = strrep (STRING, "@@VERBOSE@@", verbose);

fid = fopen (["meshfunction_", label, ".h"], "w");
if (is_valid_file_id (fid))
  fputs (fid, STRING);
  output = fclose (fid);
else
  error ("cannot open file");
  output = 1;
endif

endfunction

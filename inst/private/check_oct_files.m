## Copyright (C) 2014 Eugenio Gianniti <eugenio.gianniti@mail.polimi.it>
##
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.


## -*- texinfo -*-
## function for internal usage only
## @end deftypefn

function found = check_oct_files (ufl_name, type)
  persistent opts = {"BilinearForm",
                     "Functional",
                     "FunctionSpace",
                     "LinearForm",
                     "Problem"};

  if (! ischar (ufl_name))
    error ("check_oct_files: invalid argument");
  endif
  type = validatestring (type, opts, "check_oct_files", "type");

  [~, err, ~] = stat ([ufl_name, "_BilinearForm.oct"]);
  bilinearform = (err == 0);
  [~, err, ~] = stat ([ufl_name, "_FunctionSpace.oct"]);
  functionspace = (err == 0);
  [~, err, ~] = stat ([ufl_name, "_LinearForm.oct"]);
  linearform = (err == 0);
  [~, err, ~] = stat ([ufl_name, "_Functional.oct"]);
  functional = (err == 0);

  switch (type)
    case "BilinearForm"
      found = bilinearform;
    case "Functional"
      found = functional;
    case "FunctionSpace"
      found = functionspace;
    case "LinearForm"
      found = linearform;
    case "Problem"
      found = bilinearform && linearform && functionspace;
    otherwise
      error ("check_oct_files: unrecognized option");
  endswitch
endfunction
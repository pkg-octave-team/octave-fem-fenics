## Copyright (C) 2014-2016 Eugenio Gianniti <eugenio.gianniti@polimi.it>
##
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.


## -*- texinfo -*-
## function for internal usage only
## @end deftypefn

function save_hash (ufl_name)
  if (! ischar (ufl_name))
    error ("save_hash: wrong input argument");
  else
    filename = [ufl_name, ".ufl"];
    try
      hash_val = hash ("md5", fileread (filename));
    catch
      hash_val = md5sum (filename);
    end_try_catch
    fid = fopen ([filename, ".md5sum"], "w");
    if (is_valid_file_id (fid))
      fputs (fid, hash_val);
      fclose (fid);
    endif
  endif
endfunction

## Copyright (C) 2014 Eugenio Gianniti <eugenio.gianniti@mail.polimi.it>
##
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.


## -*- texinfo -*-
## function for internal usage only
## @end deftypefn

function found = check_mf_files (label)
  if (! ischar (label))
    error ("check_mf_files: invalid argument");
  endif

  [~, err, ~] = stat (["MeshFunction_", label, ".oct"]);
  read = (err == 0);
  [~, err, ~] = stat (["@meshfunction_", label, "/save.oct"]);
  write = (err == 0);

  found = read && write;
endfunction

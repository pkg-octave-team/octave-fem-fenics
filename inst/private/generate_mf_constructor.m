## Copyright (C) 2014 Eugenio Gianniti <eugenio.gianniti@mail.polimi.it>
##
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.


## -*- texinfo -*-
## function for internal usage only
## @end deftypefn

function output = generate_mf_constructor (label)

STRING ="\n\
#include <dolfin.h>\n\
#include <fem-fenics/mesh.h>\n\
#include ""meshfunction_@@LABEL@@.h""\n\
#include <fem-fenics/dolfin_compat.h>\n\
\n\
DEFUN_DLD (MeshFunction_@@LABEL@@, args, nargout,\
           ""MF = MeshFunction_@@LABEL@@ (TYPE, MESH, FILENAME)"")\n\
{\n\
\n\
  int nargin = args.length ();\n\
  octave_value retval;\n\
\n\
  if (nargin < 2 || nargin > 2 || nargout > 1)\n\
    print_usage ();\n\
  else\n\
    {\n\
      if (! mesh_type_loaded)\n\
        {\n\
          mesh::register_type ();\n\
          mesh_type_loaded = true;\n\
          mlock ();\n\
        }\n\
\n\
      if (! meshfunction_@@LABEL@@_type_loaded)\n\
        {\n\
          meshfunction_@@LABEL@@::register_type ();\n\
          meshfunction_@@LABEL@@_type_loaded = true;\n\
          mlock ();\n\
        }\n\
\n\
      if (args(0).type_id () == mesh::static_type_id ()\n\
          && args(1).is_string ())\n\
        {\n\
          std::string filename = args(1).string_value ();\n\
          mesh const & msh_arg =\n\
            static_cast<mesh const &> (args(0).get_rep ());\n\
\n\
          if (!error_state)\n\
            {\n\
              SHARED_PTR <dolfin::Mesh const> const &\n\
                pmsh = msh_arg.get_pmsh ();\n\
\n\
              try\n\
                { retval = new meshfunction_@@LABEL@@ (pmsh, filename); }\n\
              catch (std::runtime_error &)\n\
                { error (""error reading file""); }\n\
            }\n\
        }\n\
      else\n\
        error (""invalid input arguments"");\n\
    }\n\
\n\
  return retval;\n\
}";

STRING = strrep (STRING, "@@LABEL@@", label);

fid = fopen (["MeshFunction_", label, ".cc"], "w");
if (is_valid_file_id (fid))
  fputs (fid, STRING);
  output = fclose (fid);
else
  error ("cannot open file");
  output = 1;
endif

endfunction

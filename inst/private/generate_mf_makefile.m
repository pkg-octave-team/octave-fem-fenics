## Copyright (C) 2014 Eugenio Gianniti <eugenio.gianniti@mail.polimi.it>
##
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.


## -*- texinfo -*-
## function for internal usage only
## @end deftypefn

function output = generate_mf_makefile (label, path)

STRING ="\n\
DIR = @@PATH@@\n\
CPPFLAGS+=@@FF_CPPFLAGS@@ -g\n\
LIBS+=@@FF_LIBS@@\n\
LDFLAGS+=@@FF_LDFLAGS@@\n\
MKOCTFILE ?= @@MKOCTFILE@@\n\
FFC ?= @@FFC@@\n\
\n\
OCTFILES = MeshFunction_@@LABEL@@.oct save_@@LABEL@@.oct\n\
\n\
all : $(OCTFILES)\n\
\n\
MeshFunction_@@LABEL@@.oct: MeshFunction_@@LABEL@@.cc \
                            meshfunction_@@LABEL@@.h\n\
	CPPFLAGS='$(CPPFLAGS)' $(MKOCTFILE) $< -o $@ -I$(DIR) -I. $(LDFLAGS) $(LIBS)\n\
\n\
save_@@LABEL@@.oct: save_@@LABEL@@.cc meshfunction_@@LABEL@@.h mkmfdir\n\
	CPPFLAGS='$(CPPFLAGS)' $(MKOCTFILE) $< -o @meshfunction_@@LABEL@@/save.oct\
 -I$(DIR) -I. $(LDFLAGS) $(LIBS)\n\
\n\
mkmfdir:\n\
	mkdir -p @meshfunction_@@LABEL@@\n\
\n\
.PHONY: clean\n\
\n\
clean:\n\
	$(RM) meshfunction_@@LABEL@@.h save_@@LABEL@@.o\n\
	$(RM) MeshFunction_@@LABEL@@.o MeshFunction_@@LABEL@@.cc\n\
	$(RM) save_@@LABEL@@.cc\n\
	$(RM) Makefile_@@LABEL@@\n\
";

STRING = strrep (STRING, "@@LABEL@@", label);
STRING = strrep (STRING, "@@PATH@@", path);
STRING = strrep (STRING, "@@FF_CPPFLAGS@@", get_vars ("CPPFLAGS"));
STRING = strrep (STRING, "@@FF_LIBS@@", get_vars ("LIBS"));
STRING = strrep (STRING, "@@FF_LDFLAGS@@", get_vars ("LDFLAGS"));
STRING = strrep (STRING, "@@MKOCTFILE@@", get_compilers ("MKOCTFILE"));
STRING = strrep (STRING, "@@FFC@@", get_compilers ("FFC"));

fid = fopen (["Makefile_", label], "w");
if (is_valid_file_id (fid))
  fputs (fid, STRING);
  output = fclose (fid);
else
  error ("cannot open file");
  output = 1;
endif

endfunction

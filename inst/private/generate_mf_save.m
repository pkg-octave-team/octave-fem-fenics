## Copyright (C) 2014 Eugenio Gianniti <eugenio.gianniti@mail.polimi.it>
##
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.


## -*- texinfo -*-
## function for internal usage only
## @end deftypefn

function output = generate_mf_save (typename, label)

STRING ="\n\
#include ""meshfunction_@@LABEL@@.h""\n\
\n\
DEFUN_DLD (save, args, nargout, ""save (MESHFUNCTION, FILENAME)"")\n\
{\n\
  int nargin = args.length ();\n\
  octave_value retval;\n\
\n\
  if (nargin < 2 || nargin > 2 || nargout > 1)\n\
    print_usage ();\n\
  else\n\
    {\n\
      if (! meshfunction_@@LABEL@@_type_loaded)\n\
        {\n\
          meshfunction_@@LABEL@@::register_type ();\n\
          meshfunction_@@LABEL@@_type_loaded = true;\n\
          mlock ();\n\
        }\n\
\n\
      if (args(0).type_id () == meshfunction_@@LABEL@@::static_type_id ()\n\
          && args(1).is_string ())\n\
        {\n\
          meshfunction_@@LABEL@@ const & mf_arg =\n\
            static_cast<meshfunction_@@LABEL@@ const &>\n\
            (args(0).get_rep ());\n\
          std::string str = args(1).string_value ();\n\
\n\
          if (!error_state)\n\
            {\n\
              dolfin::MeshFunction <@@TYPENAME@@ > const &\n\
                mf = mf_arg.get_mf ();\n\
              str += "".xdmf"";\n\
              try\n\
                {\n\
                  dolfin::File file (str);\n\
                  file << mf;\n\
                }\n\
              catch (std::runtime_error &)\n\
                { error (""error saving meshfunction@@LABEL@@""); }\n\
              retval = 0;\n\
            }\n\
        }\n\
      else\n\
        error (""invalid input arguments"");\n\
    }\n\
\n\
  return retval;\n\
}";

STRING = strrep (STRING, "@@TYPENAME@@", typename);
STRING = strrep (STRING, "@@LABEL@@", label);

fid = fopen (["save_", label, ".cc"], "w");
if (is_valid_file_id (fid))
  fputs (fid, STRING);
  output = fclose (fid);
else
  error ("cannot open file");
  output = 1;
endif

endfunction

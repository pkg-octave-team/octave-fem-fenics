## Copyright (C) 2013 Marco Vassallo <gedeone-octave@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.


## -*- texinfo -*-
## function for internal usage only
## @end deftypefn

function output = generate_makefile (ufl_name, path)

STRING ="\n\
DIR = @@PATH@@\n\
CPPFLAGS+=@@FF_CPPFLAGS@@ -g\n\
LIBS+=@@FF_LIBS@@\n\
LDFLAGS+=@@FF_LDFLAGS@@\n\
MKOCTFILE ?= @@MKOCTFILE@@\n\
FFC ?= @@FFC@@\n\
\n\
OCTFILES = @@UFL_NAME@@_FunctionSpace.oct @@UFL_NAME@@_BilinearForm.oct @@UFL_NAME@@_LinearForm.oct\n\
\n\
all : $(OCTFILES)\n\
fs : @@UFL_NAME@@_FunctionSpace.oct\n\
rhs : @@UFL_NAME@@_BilinearForm.oct\n\
lhs : @@UFL_NAME@@_LinearForm.oct\n\
fun : @@UFL_NAME@@_Functional.oct\n\
\n\
@@UFL_NAME@@_FunctionSpace.oct: @@UFL_NAME@@.h @@UFL_NAME@@_FunctionSpace.cc\n\
	CPPFLAGS='$(CPPFLAGS)' $(MKOCTFILE) @@UFL_NAME@@_FunctionSpace.cc -I$(DIR) -I. $(LDFLAGS) $(LIBS)\n\
\n\
@@UFL_NAME@@_BilinearForm.oct: @@UFL_NAME@@.h @@UFL_NAME@@_BilinearForm.cc\n\
	CPPFLAGS='$(CPPFLAGS)' $(MKOCTFILE) @@UFL_NAME@@_BilinearForm.cc -I$(DIR) -I. $(LDFLAGS) $(LIBS)\n\
\n\
@@UFL_NAME@@_LinearForm.oct: @@UFL_NAME@@.h @@UFL_NAME@@_LinearForm.cc\n\
	CPPFLAGS='$(CPPFLAGS)' $(MKOCTFILE) @@UFL_NAME@@_LinearForm.cc -I$(DIR) -I. $(LDFLAGS) $(LIBS)\n\
\n\
@@UFL_NAME@@_Functional.oct: @@UFL_NAME@@.h @@UFL_NAME@@_Functional.cc\n\
	CPPFLAGS='$(CPPFLAGS)' $(MKOCTFILE) @@UFL_NAME@@_Functional.cc -I$(DIR) -I. $(LDFLAGS) $(LIBS)\n\
\n\
@@UFL_NAME@@.h: @@UFL_NAME@@.ufl\n\
	$(FFC) -l dolfin @@UFL_NAME@@.ufl\n\
\n\
.PHONY: clean\n\
\n\
clean:\n\
	$(RM) @@UFL_NAME@@_FunctionSpace.o @@UFL_NAME@@_FunctionSpace.cc @@UFL_NAME@@.h\n\
	$(RM) @@UFL_NAME@@_BilinearForm.o @@UFL_NAME@@_BilinearForm.cc\n\
	$(RM) @@UFL_NAME@@_LinearForm.o @@UFL_NAME@@_LinearForm.cc\n\
	$(RM) @@UFL_NAME@@_Functional.o @@UFL_NAME@@_Functional.cc\n\
	$(RM) Makefile_@@UFL_NAME@@\n\
";

STRING = strrep (STRING, "@@UFL_NAME@@", ufl_name);
STRING = strrep (STRING, "@@PATH@@", path);
STRING = strrep (STRING, "@@FF_CPPFLAGS@@", get_vars ("CPPFLAGS"));
STRING = strrep (STRING, "@@FF_LIBS@@", get_vars ("LIBS"));
STRING = strrep (STRING, "@@FF_LDFLAGS@@", get_vars ("LDFLAGS"));
STRING = strrep (STRING, "@@MKOCTFILE@@", get_compilers ("MKOCTFILE"));
STRING = strrep (STRING, "@@FFC@@", get_compilers ("FFC"));

fid = fopen (sprintf ("Makefile_%s", ufl_name), "w");
if (fid >= 0)
  fputs (fid, STRING);
  output = fclose (fid);
else
  error ("cannot open file");
  output = 1;
endif

endfunction

## Copyright (C) 2014-2016 Eugenio Gianniti <eugenio.gianniti@polimi.it>
##
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.


## -*- texinfo -*-
## function for internal usage only
## @end deftypefn

function differs = check_hash (ufl_name)
  if (! ischar (ufl_name))
    error ("check_hash: wrong input argument");
  else
    filename = [ufl_name, ".ufl"];
    hashfile = [filename, ".md5sum"];
    [~, err, ~] = stat (hashfile);
    if (err == -1)
      differs = true;
    else
      oldhash = fileread (hashfile);
      try
        newhash = hash ("md5", fileread (filename));
      catch
        newhash = md5sum (filename);
      end_try_catch
      differs = ! strcmp (oldhash, newhash);
    endif
  endif
endfunction

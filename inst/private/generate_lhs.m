## Copyright (C) 2013 Marco Vassallo <gedeone-octave@users.sourceforge.net>
## Copyright (C) 2016 Eugenio Gianniti <eugenio.gianniti@polimi.it>
##
## This program is free software; you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free Software
## Foundation; either version 3 of the License, or (at your option) any later
## version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
## details.
##
## You should have received a copy of the GNU General Public License along with
## this program; if not, see <http://www.gnu.org/licenses/>.


## -*- texinfo -*-
## function for internal usage only
## @end deftypefn

function output = generate_lhs (ufl_name)

STRING ="\n\
#include ""@@UFL_NAME@@.h""\n\
#include <fem-fenics/form.h>\n\
#include <fem-fenics/coefficient.h>\n\
#include <fem-fenics/function.h>\n\
#include <fem-fenics/functionspace.h>\n\
#include <fem-fenics/meshfunction.h>\n\
#include <fem-fenics/dolfin_compat.h>\n\
\n\
DEFUN_DLD (@@UFL_NAME@@_LinearForm, args, ,\
           "" b = fem_lhs_@@UFL_NAME@@ (FUNCTIONAL SPACE, COEFF)"")\n\
{\n\
  int nargin = args.length ();\n\
  octave_value retval;\n\
\n\
  if (nargin < 1)\n\
    print_usage ();\n\
  else\n\
    {\n\
\n\
      if (! functionspace_type_loaded)\n\
        {\n\
          functionspace::register_type ();\n\
          functionspace_type_loaded = true;\n\
          mlock ();\n\
        }\n\
      if (! form_type_loaded)\n\
        {\n\
          form::register_type ();\n\
          form_type_loaded = true;\n\
          mlock ();\n\
        }\n\
\n\
      if (args(0).type_id () == functionspace::static_type_id ())\n\
        {\n\
          const functionspace & fspo\n\
            = static_cast<const functionspace&> (args(0).get_rep ());\n\
\n\
          if (! error_state)\n\
            {\n\
              const SHARED_PTR <dolfin::FunctionSpace const> & V = fspo.get_pfsp ();\n\
              @@UFL_NAME@@::LinearForm L (V);\n\
              std::size_t ncoef = L.num_coefficients (), nc = 0;\n\
\n\
              if (! coefficient_type_loaded)\n\
                {\n\
                  coefficient::register_type ();\n\
                  coefficient_type_loaded = true;\n\
                  mlock ();\n\
                }\n\
\n\
              if (! function_type_loaded)\n\
                {\n\
                  function::register_type ();\n\
                  function_type_loaded = true;\n\
                  mlock ();\n\
                }\n\
\n\
              if (! meshfunction_type_loaded)\n\
                {\n\
                  meshfunction::register_type ();\n\
                  meshfunction_type_loaded = true;\n\
                  mlock ();\n\
                }\n\
\n\
              for (std::size_t i = 1; i < nargin; ++i)\n\
                {\n\
                  if (args(i).type_id () == coefficient::static_type_id ())\n\
                    {\n\
                      const coefficient & cf\n\
                        = static_cast <const coefficient&> (args(i).get_rep ());\n\
\n\
                      std::size_t n = L.coefficient_number (cf.get_str ());\n\
                      const SHARED_PTR <const expression> & pexp = cf.get_expr ();\n\
                      L.set_coefficient (n, pexp);\n\
                      ++nc;\n\
                    }\n\
\n\
                  if (args(i).type_id () == function::static_type_id ())\n\
                    {\n\
                      const function & fun\n\
                        = static_cast <const function&> (args(i).get_rep ());\n\
\n\
                      std::size_t n = L.coefficient_number (fun.get_str ());\n\
                      const SHARED_PTR <const dolfin::Function> & pfun =\
                        fun.get_pfun ();\n\
                      L.set_coefficient (n, pfun);\n\
                      ++nc;\n\
                    }\n\
\n\
                  if (args(i).type_id () == meshfunction::static_type_id ())\n\
                    {\n\
                      meshfunction const & mf_arg =\n\
                        static_cast <meshfunction const &> (args(i).get_rep ());\n\
\n\
                      std::string type = mf_arg.get_str ();\n\
                      SHARED_PTR <dolfin::MeshFunction <std::size_t> const> const &\n\
                        mf = mf_arg.get_pmf ();\n\
                      if (type == ""dx"")\n\
                        L.dx = mf;\n\
                      else if (type == ""ds"")\n\
                        L.ds = mf;\n\
                      else if (type == ""dS"")\n\
                        L.dS = mf;\n\
                    }\n\
                 }\n\
\n\
              if (nc != ncoef)\n\
                error (""Wrong number of coefficient"");\n\
              else\n\
                {\n\
                  retval = new form (L);\n\
                }\n\
            }\n\
        }\n\
    }\n\
  return retval;\n\
}";

STRING = strrep (STRING, "@@UFL_NAME@@", ufl_name);

fid = fopen (sprintf ("%s_LinearForm.cc", ufl_name), "w");
if (fid >= 0)
  fputs (fid, STRING);
  output = fclose (fid);
else
  error ("cannot open file");
  output = 1;
endif

endfunction
